from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='pavlovdog',
      version='0.1',
      description='A framework for simplifying reinforcement learning setups',
      long_description=readme(),
      url='https://gitlab.com/DestructiveReasoning/pavlovdog',
      author='Harley Wiltzer',
      author_email='harley.wiltzer@mail.mcgill.ca',
      license='MIT',
      packages=['pavlovdog'],
      include_package_data=True,
      zip_safe=False)
