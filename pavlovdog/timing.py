import datetime

class Stopwatch:
    def __init__(self):
        self._start = self._get_current_time()
        self._last = self._get_current_time()
        self._elapsed = self._last - self._start
        self._delta = self._elapsed

    def lap(self):
        t = self._get_current_time()
        self._elapsed = t - self._start
        self._delta = t - self._last
        self._last = t

    def get_elapsed(self):
        return fmt_elapsed(self._elapsed)

    def get_delta(self):
        return fmt_elapsed(self._delta)

    def _get_current_time(self):
        return datetime.datetime.now()

def fmt_elapsed(elapsed):
    return "{} min {} sec".format(int(elapsed.seconds/60), elapsed.seconds % 60)
