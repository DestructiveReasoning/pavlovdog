from unittest import TestCase

import pavlovdog.timing as T
from pavlovdog.timing import Stopwatch

import datetime

class DummyDateTime:
    def __init__(self, seconds):
        self.seconds = seconds

class DummyStopwatch(Stopwatch):
    def __init__(self):
        self._t = 0
        super(DummyStopwatch, self).__init__()

    def _get_current_time(self):
        current_time = self._t
        self._t += 1
        return datetime.datetime.fromtimestamp(current_time)

DUMMY_DATE_TIME = DummyDateTime(242)

class TestTiming(TestCase):
    def test_fmt_elapsed(self):
        formatted = T.fmt_elapsed(DUMMY_DATE_TIME)
        self.assertEqual(formatted, "4 min 2 sec")

    def test_stopwatch_elapsed(self):
        stopwatch = DummyStopwatch()
        elapsed = stopwatch.get_elapsed()
        self.assertEqual(T.fmt_elapsed(DummyDateTime(1)), elapsed)

    def test_stopwatch_delta(self):
        stopwatch = DummyStopwatch()
        delta = stopwatch.get_delta()
        self.assertEqual(T.fmt_elapsed(DummyDateTime(1)), delta)
        stopwatch.lap()
        delta = stopwatch.get_delta()
        elapsed = stopwatch.get_elapsed()
        self.assertEqual(T.fmt_elapsed(DummyDateTime(2)), elapsed)
        self.assertEqual(T.fmt_elapsed(DummyDateTime(1)), delta)
