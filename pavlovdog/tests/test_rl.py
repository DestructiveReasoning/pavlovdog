from unittest import TestCase

import pavlovdog.rl as RL

class TestRL(TestCase):
    def test_discounted_cumsum(self):
        gamma = 0.5
        rewards = [2, 4, 6, 8, 10]
        self.assertSequenceEqual([7.125, 10.25, 12.5, 13, 10], RL.discounted_cumsum(rewards, gamma))
