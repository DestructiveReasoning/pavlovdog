from unittest import TestCase

from pavlovdog.rl.replay import ReplayBuffer

class TestReplay(TestCase):
    def test_replay(self):
        buf_capacity = 3
        buf = ReplayBuffer(buf_capacity)
        buf.store(1)
        sample = buf.sample(5)
        self.assertSequenceEqual(sample, [1 for _ in range(5)])

    def test_overflow(self):
        buf_capacity = 3
        buf = ReplayBuffer(buf_capacity)
        for _ in range(buf_capacity):
            buf.store(1)
        for _ in range(buf_capacity):
            buf.store(2)
        sample = buf.sample(5)
        self.assertSequenceEqual(sample, [2 for _ in range(5)])
