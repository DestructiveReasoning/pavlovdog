from unittest import TestCase

from pavlovdog.neuralnets import MLP
from pavlovdog.neuralnets import GaussianMLP

import torch
import numpy as np

DUMMY_INPUT_TENSOR_SCALAR = torch.FloatTensor([0.0])

class TestNeuralNets(TestCase):
    def test_mlp_pipeline(self):
        mlp = MLP([1, 1, 2], output_activation=lambda x: 1)
        self.assertEqual(1, mlp.forward(DUMMY_INPUT_TENSOR_SCALAR))

    def test_gaussian_mlp_pipeline(self):
        output_dim = 2
        mlp = GaussianMLP([1, 1, 2], output_dim, output_activation=lambda x: np.zeros(output_dim))
        mu, var = mlp.forward(DUMMY_INPUT_TENSOR_SCALAR)
        self.assertSequenceEqual(tuple(np.zeros(output_dim)), tuple(mu))
        self.assertSequenceEqual((output_dim,), tuple(var.shape))
