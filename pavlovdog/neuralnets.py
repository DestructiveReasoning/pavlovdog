import torch
import torch.nn as nn

class MLP(nn.Module):
    def __init__(self, layer_sizes, activation=lambda x: x.tanh(), output_activation=None):
        super(MLP, self).__init__()
        self._layers = nn.ModuleList()
        self._init_layers(layer_sizes)
        self._activation = activation
        self._output_activation = output_activation

    def forward(self, x):
        for layer in self._layers:
            x = layer(x)
            if self._activation is not None:
                x = self._activation(x)
        if self._output_activation is not None:
            x = self._output_activation(x)
        return x

    def _init_layers(self, layer_sizes):
        assert len(layer_sizes) >= 2, "MLP must have at least 2 layer sizes: input layer and output layer"
        last_out_size = layer_sizes[0]
        for layer_size in layer_sizes[1:]:
            self._layers.append(nn.Linear(last_out_size, layer_size))
            last_out_size = layer_size

class GaussianMLP(nn.Module):
    def __init__(self, layer_sizes, out_dim, activation=lambda x: x.tanh(), output_activation=None):
        super(GaussianMLP, self).__init__()
        self._out_dim = out_dim
        self._layers = nn.ModuleList()
        self._init_layers(layer_sizes)
        self._mu = nn.Linear(layer_sizes[-1], self._out_dim)
        self._logvar = nn.Linear(layer_sizes[-1], self._out_dim)
        self._activation = activation
        self._output_activation = output_activation

    def _init_layers(self, layer_sizes):
        last_out_size = layer_sizes[0]
        for layer_size in layer_sizes[1:]:
            self._layers.append(nn.Linear(last_out_size, layer_size))
            last_out_size = layer_size

    def forward(self, x):
        for layer in self._layers:
            x = layer(x)
            if self._activation is not None:
                x = self._activation(x)
        mu = self._mu(x)
        logvar = self._logvar(x)
        if self._output_activation is not None:
            mu = self._output_activation(mu)
        var = logvar.exp()
        return mu, var
