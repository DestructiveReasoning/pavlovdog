import torch
import numpy as np

import pavlovdog.rl as RL

from pavlovdog.timing import Stopwatch

class RLAgent:
    def __init__(self, env_fn, brain):
        self._env_fn = env_fn
        self._brain = brain
        self._env = env_fn()

    def train(self, verbose=True, lr=1e-3, epochs=100, runs_per_epoch=30, max_steps=1000, gamma=0.99, win_condition=None, **kwargs):
        self.timer = Stopwatch()
        self.pre_train_setup()

        for epoch in range(epochs):
            contexts = []
            for run in range(runs_per_epoch):
                context = RLContext()
                for _ in range(max_steps):
                    if self.step(context).done():
                        break
                contexts.append(context)
            loss = self.learn(contexts, gamma)
            if verbose:
                self.timer.lap()
                print(self.show_progress(epoch + 1, epochs, contexts, loss.item()))
            if win_condition is not None:
                epoch_score = mean_epoch_reward(contexts)
                if epoch_score >= win_condition:
                    if verbose:
                        print("Goal achieved!")
                        print("A score of {} (> {}) was achieved in epoch {}.".format(epoch_score, win_condition, epoch + 1))
                    break

    def learn(self, contexts, gamma):
        loss = self.loss(contexts, gamma)
        self.policy_optimizer.zero_grad()
        loss.backward()
        self.policy_optimizer.step()
        return loss

    def loss(self, contexts, gamma, **kwargs):
        returns = self.process_returns(contexts, gamma=gamma)
        losses = []
        for (rets, context) in zip(returns, contexts):
            rets = np.array(rets)
            logp_tensor = torch.stack([logp.gather(-1, action) for (logp, action) in zip(context.logps_buf(), context.action_buf())])
            losses.append(logp_tensor.dot(torch.from_numpy(rets).float()))
        return -torch.stack(losses).mean()

    def step(self, context):
        obs = self._env.reset() if context.has_no_context() else context.next_obs()
        action, logps = self.sample_action(obs)
        next_obs, rew, done, _ = self._env.step(action.item())
        context.add(obs, logps, action, rew, done, next_obs)
        return context

    def sample_action(self, obs):
        state = torch.from_numpy(obs.flatten()).float()
        action_probs = self._brain(state)
        logps = action_probs.log()
        action = action_probs.multinomial(1).squeeze()
        return action, logps

    def process_returns(self, contexts, gamma=0.99, **kwargs):
        return [RL.discounted_cumsum(context.rew_buf(), gamma) for context in contexts]

    def pre_train_setup(self, lr=1e-3, **kwargs):
        self.policy_optimizer = torch.optim.Adam(self._brain.parameters(), lr=lr)

    def show_progress(self, cur_epoch, total_epochs, contexts, loss):
        mean_reward = mean_epoch_reward(contexts)
        pct = int(float(cur_epoch) * 100.0 / total_epochs)
        fmt_string = "Epoch {:>5} ({:>3}%) :: Mean reward: {:>8.2f} | Loss: {:>13.6f} | Elapsed: {}"
        return fmt_string.format(cur_epoch, pct, mean_reward, loss, self.timer.get_elapsed())

    def show_preview(self, max_steps=1000):
        env = self._env_fn()
        obs = env.reset()
        score = 0
        for _ in range(max_steps):
            action, _ = self.sample_action(obs)
            obs, rew, done, _ = env.step(action.item())
            score += rew
            env.render()
            if done:
                break
        print("Preview score: {}".format(score))
        env.close()

class RLContext:
    def __init__(self):
        self._obs_buf = []
        self._logps_buf = []
        self._action_buf = []
        self._rew_buf = []
        self._done_buf = []
        self._next_obs_buf = []
        self._total_reward = 0.0

    def has_no_context(self):
        return len(self._obs_buf) == 0

    def add(self, obs, logps, action, rew, done, next_obs):
        self._obs_buf.append(obs)
        self._logps_buf.append(logps)
        self._action_buf.append(action)
        self._rew_buf.append(rew)
        self._done_buf.append(done)
        self._next_obs_buf.append(next_obs)
        self._total_reward += rew

    def current(self):
        if len(self._obs_buf) == 0:
            raise Exception("Cannot get current context from empty RLContext")
        obs = self.obs()
        logps = self.logps()
        action = self.action()
        rew = self.rew()
        done = self.done()
        next_obs = self.next_obs()
        return obs, logps, action, rew, done, next_obs()

    def obs(self):
        if len(self._obs_buf) == 0:
            raise Exception("Cannot get current observation from empty RLContext")
        return self._obs_buf[-1]

    def logps(self):
        if len(self._logps_buf) == 0:
            raise Exception("Cannot get current logps from empty RLContext")
        return self._logps_buf[-1]

    def action(self):
        if len(self._action_buf) == 0:
            raise Exception("Cannot get current action from empty RLContext")
        return self._action_buf[-1]

    def rew(self):
        if len(self._rew_buf) == 0:
            raise Exception("Cannot get current reward from empty RLContext")
        return self._action_buf[-1]

    def done(self):
        if len(self._done_buf) == 0:
            raise Exception("Cannot get current done flag from empty RLContext")
        return self._done_buf[-1]

    def next_obs(self):
        if len(self._next_obs_buf) == 0:
            raise Exception("Cannot get current next observation from empty RLContext")
        return self._next_obs_buf[-1]

    def obs_buf(self):
        return self._obs_buf

    def logps_buf(self):
        return self._logps_buf

    def action_buf(self):
        return self._action_buf

    def rew_buf(self):
        return self._rew_buf

    def total_reward(self):
        return self._total_reward

def mean_epoch_reward(contexts):
        return np.mean([context.total_reward() for context in contexts])
