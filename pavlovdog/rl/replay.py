import numpy as np

class ReplayBuffer:
    def __init__(self, capacity):
        self._capacity = capacity
        self._buffer = []
        self._ptr = 0

    def store(self, item):
        if len(self._buffer) < self._capacity:
            self._buffer.append(item)
        else:
            self._buffer[self._ptr] = item
        self._ptr = (self._ptr + 1) % self._capacity

    def sample(self, sample_size):
        indices = np.random.choice(list(range(len(self._buffer))), size=(sample_size,))
        return [self._buffer[i] for i in indices]
