def discounted_cumsum(xs, gamma):
    a = xs[::-1]
    for i in range(len(a) - 1):
        a[i + 1] += gamma * a[i]
    return a[::-1]
