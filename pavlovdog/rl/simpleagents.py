import numpy as np

from pavlovdog.neuralnets import MLP
from pavlovdog.rl.agent import RLAgent

class MLPAgent(RLAgent):
    def __init__(self, env_fn, hidden_sizes, activation=lambda x: x.relu()):
        env = env_fn()
        obs_space = env.observation_space
        act_space = env.action_space
        act_dim = act_space.n
        obs_dim = np.prod(obs_space.shape)
        brain = MLP([obs_dim] + hidden_sizes + [act_dim], activation=activation, output_activation=lambda x: x.softmax(-1))
        super(MLPAgent, self).__init__(env_fn, brain)
